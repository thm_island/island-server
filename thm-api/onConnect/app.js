const AWS = require('aws-sdk');

exports.onConnect = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
        console.log(`onConnect() - ${connectionId}`);
    }

    return { statusCode: 200, body: 'Connected.' };
};