const Responses = require('responses');
const Product = require('product');
const Security = require('security');
const AWS = require('aws-sdk');

exports.searchProducts = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const searchText = body.searchText;
    const sessionId = body.sessionId;
    const service = body.service;
    const message = body.message;

    console.log(`searchProducts: ${JSON.stringify(searchText)}`);

    var response = Responses.errorResponse();

    var authResponse = await Security.isAuthorized(sessionId);
    if (authResponse.status.code === Responses.CODE_SUCCESS) {
        console.log(`Calling create search products now...`);
        response = await Product.search(searchText);
    }
    else {
        console.log(`Error searching products`);
        response.status.description = "The user is not authorized.";
    }
    
    response.service = service;
    response.message = message;

    if (connectionId) {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });

        await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
    }

    //standard http com, no web socket push needed
    return response;
};