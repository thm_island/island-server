const AWS = require('aws-sdk');
const Room = require('room');

exports.addUserToRoom = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const room = body.room;
    const username = body.username;

    var response = await Room.addUser(room, username, connectionId);

    if(connectionId)
    {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });
        response.message = 'addUserToRoomResponse';

        if(response.roomUsers)
        {
            for (var i = 0; i < response.roomUsers.length; i++) {
                const roomUser = response.roomUsers[i];
                if (roomUser.connectionId) {

                    try
                    {
                        await apigwManagementApi.postToConnection({ ConnectionId: roomUser.connectionId, Data: JSON.stringify(response) }).promise();
                    }
                    catch(err)
                    {
                        console.log(`Error while trying to send ws message to ${roomUser.connectionId} with username ${roomUser.roomUsername}: ${JSON.stringify(err)}`);
                        Room.removeUser(room, roomUser.roomUsername);
                    }
                }
            }
        }
    }

    return response;
};