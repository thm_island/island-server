const AWS = require('aws-sdk');
const GameLiftUtil = require('gamelift');

exports.startSinglePlayerGame = async (event) => {


    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    //const body = JSON.parse(event.body);

    var response = await GameLiftUtil.startSinglePlayerGame();

    if(connectionId)
    {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });
        response.message = 'startSinglePlayerGameResponse';

        try {
            await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
        }
        catch (err) {
            console.log(`Error while trying to send ws message to ${connectionId}: ${JSON.stringify(err)}`);
        }
    }

    return response;
};