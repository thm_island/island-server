#!/bin/bash
set -eo pipefail
aws cloudformation package --template-file template.yml --s3-bucket thm.island.deployment --output-template-file out.yml --profile thm-deployment
aws cloudformation deploy --template-file out.yml --stack-name thm-island --capabilities CAPABILITY_NAMED_IAM --profile thm-deployment
