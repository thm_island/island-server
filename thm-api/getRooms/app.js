const AWS = require('aws-sdk');
const Room = require('room');

exports.getRooms = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    //const body = JSON.parse(event.body);

    var response = await Room.getRooms();

    if(connectionId)
    {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });
        response.message = 'getRoomsResponse';

        await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
    }

    return response;
};