const AWS = require('aws-sdk');
const Room = require('room');

exports.onDisconnect = async (event) => {

    console.log(`onDisconnect() - start`);

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    console.log(`onDisconnect() - removeConnection calling ${connectionId}`);
    var response = await Room.removeConnection(connectionId);
    console.log(`onDisconnect() - removeConnection called ${connectionId}`);
    
    //inform all users in the room that a user has been removed
    if(connectionId)
    {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });
        response.message = 'removeUserFromRoomResponse';

        if(response.roomUsers)
        {
            for (var i = 0; i < response.roomUsers.length; i++) {
                const roomUser = response.roomUsers[i];
                if (roomUser.connectionId) {

                    try
                    {
                        await apigwManagementApi.postToConnection({ ConnectionId: roomUser.connectionId, Data: JSON.stringify(response) }).promise();
                    }
                    catch(err)
                    {
                        console.log(`Error while trying to send ws message to ${roomUser.connectionId} with username ${roomUser.roomUsername}: ${JSON.stringify(err)}`);
                        Room.removeUser(room, roomUser.roomUsername);
                    }
                }
            }
        }
    }

    console.log(`onDisconnect() - end`);

    return { statusCode: 200, body: 'Disconnected.' };
};