const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-west-2' });

exports.requestPartyWithFriends = async event => {
    let connectionData;

    console.log(`we are in the request party with friends function`);

    try {
        connectionData = await ddb.scan({ TableName: 'WebAPIConnections', ProjectionExpression: 'connectionId' }).promise();
    } catch (e) {
        return { statusCode: 500, body: e.stack };
    }

    console.log(`api endpoint ${event.requestContext.domainName}/${event.requestContext.stage}`);

    const apigwManagementApi = new AWS.ApiGatewayManagementApi({
        endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
    });

    const postData = JSON.parse(event.body).data;

    const postCalls = connectionData.Items.map(async ({ connectionId }) => {
        try {
            console.log(`posting to client ${connectionId} with data ${JSON.stringify(postData)}`);
            await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: postData }).promise();
        } catch (e) {
            console.log(`exception ${JSON.stringify(e)}`);
            if (e.statusCode === 410) {
                console.log(`Found stale connection, deleting ${connectionId}`);
                await ddb.delete({ TableName: 'WebAPIConnections', Key: { connectionId } }).promise();
            } else {
                throw e;
            }
        }
    });

    try {
        await Promise.all(postCalls);
    } catch (e) {
        return { statusCode: 500, body: e.stack };
    }

    return { statusCode: 200, body: 'Party Request sent to Friends' };
};