const Responses = require('responses');
const Product = require('product');
const Security = require('security');
const AWS = require('aws-sdk');

exports.createProduct = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const product = body.product;
    const sessionId = body.sessionId;
    const service = body.service;
    const message = body.message;

    console.log(`createProduct: ${JSON.stringify(product)}`);

    var response = Responses.errorResponse();

    var authResponse = await Security.isAuthorized(sessionId);
    if (authResponse.status.code === Responses.CODE_SUCCESS) {
        console.log(`Calling create prooduct now...`);
        response = await Product.create(product);
    }
    else {
        console.log(`Error creating product`);
        response.status.description = "The user is not authorized.";
    }
    
    response.service = service;
    response.message = message;

    if (connectionId) {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });

        await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
    }

    //standard http com, no web socket push needed
    return response;
};