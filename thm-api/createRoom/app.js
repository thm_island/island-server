const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient({ region: 'us-west-2' });

const uuidv4 = require('uuid/v4');
const crypto = require('crypto');
const Responses = require('responses');
const Helper = require('helper');
const User = require('user');

exports.createRoom = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const room = body.room;

    //console.log(`init apigw managmeent api`);

    const apigwManagementApi = new AWS.ApiGatewayManagementApi({
        endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
    });

    var response = await Room.create(room);

    response.message = 'createRoomResponse';

    //console.log(`post response`);

    await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();

    //console.log(`return ok`);

    return { statusCode: 200, body: 'Returning from create user' };
    //return response;
};