const AWS = require('aws-sdk');
const uuidv4 = require('uuid/v4');
const crypto = require('crypto');
const Responses = require('responses');
const Helper = require('helper');
const User = require('user');

exports.createUser = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const service = body.service;
    const message = body.message;
    const user = body.user;

    const connectedUser = {
        ...user,
        connectionId
    };

    var response = await User.create(connectedUser);

    response.service = service;
    response.message = message;

    if (connectionId) {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });

        await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
    }
    
    return response;
};