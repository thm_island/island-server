const AWS = require('aws-sdk');
const Room = require('room');

exports.startRoomGame = async (event) => {


    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const room = body.room;
    const username = body.username;

    console.log(`Start room game`);
    var response = await Room.startGame(room);

    
    console.log(`Start room game response ${JSON.stringify(response)}`);

    if(connectionId)
    {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });
        response.message = 'startRoomGameResponse';

        if(response.roomUsers)
        {
            //inform the users that the game is going to start,
            //this includes the gamelift game session id
            for(var i=0; i<response.roomUsers.length; i++)
            {
                const roomUser = response.roomUsers[i];
                var currentConnectionId = roomUser.connectionId;
                if(roomUser.roomUsername === username)
                {
                    // use the most recent connection id for the user
                    // starting the the game. This will ensure that
                    // the game will be started for at 
                    // least one player
                    currentConnectionId = connectionId;
                }
                if (currentConnectionId)
                {
                    try
                    {
                        console.log(`Notify ${roomUser.roomUsername}, ${currentConnectionId} of start game response`);
                        await apigwManagementApi.postToConnection({ ConnectionId: currentConnectionId, Data: JSON.stringify(response) }).promise();
                    }
                    catch(err)
                    {
                        console.log(`Error while trying to send ws message to ${currentConnectionId} with username ${roomUser.roomUsername}: ${JSON.stringify(err)}`);
                        Room.removeUser(room, roomUser.roomUsername);
                    }
                }
            }
        }
        else
        {
            console.log(`No room users to send to...`);
            await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
        }
    }

    return response;
};