const AWS = require('aws-sdk');
const User = require('user');
const Responses = require('responses');
const Security = require('security');

exports.searchUsers = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const message = body.message;
    const service = body.service;
    const searchText = body.searchText;
    const pageSize = body.pageSize;
    const lastEvaluatedKey = body.lastEvaluatedKey;
    const sessionId = body.sessionId;

    var response = Responses.errorResponse();

    var authResponse = await Security.isAuthorized(sessionId);
    if (authResponse.status.code === Responses.CODE_SUCCESS) {
        var response = await User.search(searchText, pageSize, lastEvaluatedKey);
    }
    else {
        response.status.description = "The user is not authorized.";
    }

    //web socket response...
    if (connectionId) {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });

        response.service = service;
        response.message = message;

        //console.log(`post response`);

        await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
    }

    //standard http com, no web socket push needed
    return response;
};