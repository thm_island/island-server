const AWS = require('aws-sdk');
const Responses = require('responses');
const User = require('user');
const Security = require('security');

exports.requestFriend = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const user = body.user;
    const sessionId = body.sessionId;
    const friendName = body.friendName;

    const connectedUser = {
        ...user,
        connectionId
    };

    //console.log(`init apigw managmeent api`);

    const apigwManagementApi = new AWS.ApiGatewayManagementApi({
        endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
    });

    var response = Responses.errorResponse();
    var authResponse = await Security.isAuthorized(sessionId, user);
    if (authResponse.status.code === Responses.CODE_SUCCESS) {
        var response = await User.respondToFriendRequest(connectedUser, friendName);
    }
    else {
        response.status.description = "The user is not authorized.";
    }

    response.message = 'acceptOrRejectFriendResponse';

    //console.log(`post response`);

    await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();

    //send friend request immediately to friend (if online)
    if(response.friendUser && response.friendUser.connectionId)
    {
        let friendRequest = Responses.successResponse();
        friendRequest.message = "respondToFriendRequest";
        friendRequest.friendUser = user;
        await apigwManagementApi.postToConnection({ ConnectionId: response.friendUser.connectionId, Data: JSON.stringify(friendRequest) }).promise();
    }

    //console.log(`return ok`);

    return { statusCode: 200, body: 'Returning from create user' };
    //return response;
};