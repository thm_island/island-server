const User = require('user');
const AWS = require('aws-sdk');

exports.authenticate = async (event) => {
    
    let connectionId;
    if(event.requestContext && event.requestContext.connectionId)
    {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const message = body.message;
    const service = body.service;
    const user = body.user;

    const connectedUser = {
        ...user,
        connectionId
    };
    
    var response = await User.authenticate(connectedUser);

    //web socket response...
    if (connectionId)
    {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });

        response.service = service;
        response.message = message;

        //console.log(`post response`);

        await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
    }

    return response;
};