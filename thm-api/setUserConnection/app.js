/*
const User = require('../service-layer/nodejs/node_modules/user');
const Security = require('../service-layer/nodejs/node_modules/security');
const Responses = require('../service-layer/nodejs/node_modules/responses');
const AWS = require('../service-layer/nodejs/node_modules/aws-sdk');
*/
const User = require('user');
const Security = require('security');
const Responses = require('responses');
const AWS = require('aws-sdk');

exports.setUserConnection = async (event) => {
    let connectionId;
    if(event.requestContext && event.requestContext.connectionId)
    {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const user = body.user;
    const sessionId = body.sessionId;

    const connectedUser = {
        ...user,
        connectionId
    };

    const apigwManagementApi = new AWS.ApiGatewayManagementApi({
        endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
    });

    var response = Responses.errorResponse();
    var authResponse = await Security.isAuthorized(sessionId, user);
    if (authResponse.status.code === Responses.CODE_SUCCESS)
    {
        var response = await User.setConnection(connectedUser);
    }
    else
    {
        response.status.description = "The user is not authorized.";
    }

    response.message = "setUserConnectionResponse";
    //console.log(`post response`);

    await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();

    //console.log(`return ok`);

    return { statusCode: 200, body: 'Returning from authenticate' };
    //return response;
};

//ssetUserConnection({ body: { user: { userId: '', }, sessionId: '2dd91fd8-2e10-42da-bffa-f370f252fdd1' }});