const AWS = require('aws-sdk');
const Room = require('room');

exports.removeUserFromRoom = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const room = body.room;
    const username = body.username;

    var response = await Room.removeUser(room, username);

    //inform all users in the room that a user has been removed
    if(connectionId)
    {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });
        response.message = 'removeUserFromRoomResponse';

        if(response.roomUsers)
        {
            for (var i = 0; i < response.roomUsers.length; i++) {
                const roomUser = response.roomUsers[i];
                if (roomUser.connectionId) {

                    try
                    {
                        await apigwManagementApi.postToConnection({ ConnectionId: roomUser.connectionId, Data: JSON.stringify(response) }).promise();
                    }
                    catch(err)
                    {
                        console.log(`Error while trying to send ws message to ${roomUser.connectionId} with username ${roomUser.roomUsername}: ${JSON.stringify(err)}`);
                        Room.removeUser(room, roomUser.roomUsername);
                    }
                }
            }
        }
    }

    return response;
};