
REM stack/stage (ex: dev, staging, prod)
set stackName=%1
set s3DeploymentBucket=%2
set awsProfile=%3
set serviceRole=%4
set database=%5
set fleetAliasId=%6
set productBucket=%7
set imageBucket=%8

rd /S /Q .\deploy-temp
mkdir deploy-temp
move .\service-layer\nodejs\node_modules\*.js .\deploy-temp\
cd .\service-layer\nodejs
call npm i
cd ..\..
move .\deploy-temp\*.js .\service-layer\nodejs\node_modules\
rd /S /Q .\deploy-temp
aws cloudformation package --template-file template.yml --s3-bucket %s3DeploymentBucket% --output-template-file out.yml --profile %awsProfile%
aws cloudformation deploy --template-file out.yml --parameter-overrides stackName=%stackName% serviceRole=%serviceRole% database=%database% fleetAliasId=%fleetAliasId% productBucket=%productBucket% imageBucket=%imageBucket% --stack-name %stackName% --capabilities CAPABILITY_NAMED_IAM --profile %awsProfile%