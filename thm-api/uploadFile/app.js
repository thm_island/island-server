const AWS = require('aws-sdk');
const Product = require('product');
const Responses = require('responses');
const Security = require('security');

exports.uploadFile = async (event) => {

    let connectionId;
    if (event.requestContext && event.requestContext.connectionId) {
        connectionId = event.requestContext.connectionId;
    }

    const body = JSON.parse(event.body);
    const message = body.message;
    const target = body.target;
    const service = body.service;
    const sessionId = body.sessionId;
    const totalChunks = body.totalChunks;
    const chunk = body.chunk;
    const ordinal = body.ordinal;
    const product = body.product;
    const merge = body.merge;
    const uploadSessionId = body.uploadSessionId;

    var response = Responses.errorResponse();

    //console.log(`entering upload bundle...chunk is ${chunk}`, body);
    var authResponse = await Security.isAuthorized(sessionId);
    if (authResponse.status.code === Responses.CODE_SUCCESS) {
        var response = await Product.uploadFile(target, ordinal, totalChunks, chunk, product, merge, uploadSessionId);
    }
    else {
        response.status.description = "The user is not authorized.";
    }

    //web socket response...
    if (connectionId) {
        const apigwManagementApi = new AWS.ApiGatewayManagementApi({
            endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
        });

        response.service = service;
        response.message = message;
        response.target = target;

        //console.log(`post response`);

        await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: JSON.stringify(response) }).promise();
    }

    //standard http com, no web socket push needed
    return response;
};